package com.example.demoapp

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import com.example.demoapp.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_picker.*

class Picker : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)
        val setColor = listOf(seek_bar, seek_bar2, seek_bar3)
        setColor.forEach() {
            it.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    fromUser: Boolean
                ) {
                    text_view.text = seek_bar.progress.toString()
                    text_view2.text = seek_bar2.progress.toString()
                    text_view3.text = seek_bar3.progress.toString()
                    setColorChange()
                }
                override fun onStartTrackingTouch(seekBar: SeekBar) {
                }
                override fun onStopTrackingTouch(seekBar: SeekBar) {
                }
            })
        }
        setColorChange()
    }
    fun setColorChange() {
        view.setBackgroundColor(Color.rgb(seek_bar.progress, seek_bar2.progress, seek_bar3.progress))
    }
}