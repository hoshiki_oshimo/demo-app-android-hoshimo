package com.example.demoapp
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_view_pager.*


class ViewPager : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragmentList = arrayListOf<Fragment>(
            Fragment1(),
            Fragment2()
        )
        val adapter = PagerAdapter(supportFragmentManager, fragmentList)
        viewPager.adapter = adapter
    }
}