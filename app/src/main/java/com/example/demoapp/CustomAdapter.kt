package com.example.demoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_custom_adapter.view.*

class CustomAdapter(private val customList: Array<String>) : RecyclerView.Adapter<CustomAdapter.CustomViewHolder>(){

    lateinit var listener: OnItemClickListener
    class CustomViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val sampleTxt = view.sampleTxt
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val item = layoutInflater.inflate(R.layout.activity_custom_adapter, parent, false)
        return CustomViewHolder(item)
    }
    override fun getItemCount(): Int {
        return customList.size
    }
    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.sampleTxt.text = customList[position]
        // タップしたとき
        holder.view.setOnClickListener {
            listener.onItemClickListener(it, position, customList[position])
        }
    }

    interface OnItemClickListener{
        fun onItemClickListener(view: View, position: Int, clickedText: String)
    }
    fun setOnItemClickListener(listener: OnItemClickListener){
        this.listener = listener
    }
}