package com.example.demoapp
import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity


class WebView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val webView = WebView(this)
        setContentView(webView)
        webView.loadUrl("https://www.sonix.asia/")
    }
}