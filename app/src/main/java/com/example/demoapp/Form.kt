package com.example.demoapp
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity

class Form: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)


        val btClick = findViewById<Button>(R.id.button)
        val listener = ClickListener()
        btClick.setOnClickListener(listener)
    }
    private inner class ClickListener : View.OnClickListener {
        override fun onClick(v: View?) {
            val name = findViewById<EditText>(R.id.nameText)
//性別の部分
            var groupButton = ""
            val gender_group: RadioGroup
            val manButton: RadioButton
            val womenButton: RadioButton
            val otherButton: RadioButton
            gender_group = findViewById(R.id.gender_group)
            manButton = findViewById(R.id.manButton)
            womenButton = findViewById(R.id.womenButton)
            otherButton = findViewById(R.id.otherButton)
            if (gender_group.checkedRadioButtonId != -1) {
                if (manButton.isChecked)
                    groupButton = "man"
                else if (womenButton.isChecked)
                    groupButton = "women"
                else if (otherButton.isChecked)
                    groupButton = "other"
            }
//生年月日の部分
//チェックボックスの部分
            var check1 = ""
            var check2 = ""
            var check3 = ""
            var check4 = ""
            var check5 = ""
            val checkbox = findViewById<CheckBox>(R.id.checkBox)
            checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                if (checkbox.isChecked) {
                    check1 = "true"

                }
            }
            //スピナーに関する処理
            val spinner1 = findViewById<Spinner>(R.id.spinner)
            val spinner2 = findViewById<Spinner>(R.id.spinner2)
            val spinner3 = findViewById<Spinner>(R.id.spinner3)
            val spin1 = spinner1.selectedItem as String
            val spin2 = spinner2.selectedItem as String
            val spin3 = spinner3.selectedItem as String
            val yearNumber: Int = Integer.parseInt(spin1)
            val monthNumber: Int = Integer.parseInt(spin2)
            val dayNumber: Int = Integer.parseInt(spin3)
            var birthDay: Int = yearNumber * 10000 + monthNumber * 100 + dayNumber
//値の受け渡し
            //Intentのインスタンスを作成
            val intent = Intent(applicationContext, Check::class.java)
            //intent変数をつなげる(第一引数はキー，第二引数は渡したい変数)
            intent.putExtra("NAME", name.text.toString())
            intent.putExtra("CHECK1", check1)
            intent.putExtra("CHECK2", check2)
            intent.putExtra("CHECK3", check3)
            intent.putExtra("CHECK4", check4)
            intent.putExtra("CHECK5", check5)
            intent.putExtra("GENDER", groupButton)
            //画面遷移を開始
            startActivity(intent)
        }
    }
}