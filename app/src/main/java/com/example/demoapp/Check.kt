package com.example.demoapp
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class Check : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check)
        val name = intent.getStringExtra("NAME")
        val check1 = intent.getStringExtra("CHECK1")
        val check2 = intent.getStringExtra("CHECK2")
        val check3 = intent.getStringExtra("CHECK3")
        val check4 = intent.getStringExtra("CHECK4")
        val check5 = intent.getStringExtra("CHECK5")
        val gender = intent.getStringExtra("GENDER")
        val nameBox= findViewById<TextView>(R.id.nameAnswer)
        var genderBox= findViewById<TextView>(R.id.genderAnswer)
        var questionBox1= findViewById<TextView>(R.id.question1)
        var questionBox2= findViewById<TextView>(R.id.question2)
        var questionBox3= findViewById<TextView>(R.id.question3)
        var questionBox4= findViewById<TextView>(R.id.question4)
        var questionBox5= findViewById<TextView>(R.id.question5)
        nameBox.text = name
        when {
            gender == "man" -> {genderBox.text = "男性"}
            gender == "women" -> {genderBox.text = "女性"}
            gender == "other" -> {genderBox.text = "その他"}
        }
        if (check1 == "true") {
            questionBox1.text = "インターネット"
        }
        if (check2 == "true") {
            questionBox2.text = "雑誌記事"
        }
        if (check3 == "true") {
            questionBox3.text = "友人・知人から"
        }
        if (check4 == "true") {
            questionBox4.text = "セミナー"
        }
        if (check5 == "true") {
            questionBox5.text = "その他"
        }
    }
    fun onBackButtonClick(view: View?) {
        finish()
    }
}